package com.bosssoft.hr.train.jsp.example.tag;

import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.io.StringWriter;

/**
 * @description: 定义<boss:userTag /> 标签
 * @author: Administrator
 * @create: 2020-05-29 13:50
 * @since
 **/
public class UserTag extends TagSupport {
    StringWriter stringWriter=new StringWriter();
    public void showLoginUser() throws IOException {
        HttpSession session=pageContext.getSession();
        JspWriter out=pageContext.getOut();
        int id= (int) session.getAttribute("id");
        String name= (String) session.getAttribute("name");
        out.println("当前登录用户id；"+id+",用户名:"+name);
    }
}
