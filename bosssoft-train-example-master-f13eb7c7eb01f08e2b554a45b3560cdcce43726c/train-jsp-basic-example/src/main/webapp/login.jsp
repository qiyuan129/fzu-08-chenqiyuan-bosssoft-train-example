<%--
  Created by IntelliJ IDEA.
  User: 62706
  Date: 2020/7/14
  Time: 23:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
</head>
<script type="text/javascript">

    function postLoginRequest(){
        // 异步对象
        var xhr = new XMLHttpRequest();

        // 设置属性
        xhr.open('post', '/loginServlet');

        // 如果想要使用post提交数据,必须添加此行
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

        var id_text=document.getElementById("user_id");
        var password_text=document.getElementById("user_password");

        // 将数据通过send方法传递
        xhr.send("id="+id_text.value+"&password="+password_text.value);

        // 发送并接受返回值
        xhr.onreadystatechange = function () {
            // 这步为判断服务器是否正确响应
            if (xhr.readyState == 4 && xhr.status == 200) {
                if(xhr.responseText=="login success"){
                    window.location.href="http://localhost:8080/user/listUser";
                }
                else{
                    alert("登录失败");
                }
            }
        };
    }
</script>
<body>
<h1>登陆界面</h1>
<%--<%--%>
<%--    if(session.isNew()==false){--%>
<%--        response.sendRedirect("/user/listUser");--%>
<%--    }--%>
<%--%>--%>
<label>用户id</label>
<input type="text" name="id" id="user_id"/><br>
<label>用户密码</label>
<input type="text" name="password" id="user_password"/>
<input type="button" value="发送登录ajax请求" id='btnAjax' onclick="postLoginRequest()">
<p>
    提示：初始有3个账户有密码的账户，用户id分别为：123、124，125<br>
    初始密码均为123456
</p>
</body>
</html>
