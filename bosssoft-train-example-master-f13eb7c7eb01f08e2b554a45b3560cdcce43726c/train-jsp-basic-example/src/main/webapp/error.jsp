<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: 62706
  Date: 2020/7/16
  Time: 16:30
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>错误界面</title>
</head>
<body>
<h2>错误信息：</h2>
<c:out value="${errorMessage}"></c:out>
<a href="/user/listUser">返回用户管理界面</a>
</body>
</html>
