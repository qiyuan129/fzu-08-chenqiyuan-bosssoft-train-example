<%--
  Created by IntelliJ IDEA.
  User: 62706
  Date: 2020/7/15
  Time: 20:23
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <title>注销中，请等待跳转</title>
</head>

<body>
<%
    session.removeAttribute("id");
    session.invalidate();
    response.sendRedirect("/login.jsp");
%>
</body>
</html>
