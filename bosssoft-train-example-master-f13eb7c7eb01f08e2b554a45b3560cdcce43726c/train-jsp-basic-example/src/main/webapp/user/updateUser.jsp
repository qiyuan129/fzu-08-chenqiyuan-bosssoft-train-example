<%--
  Created by IntelliJ IDEA.
  User: 62706
  Date: 2020/7/16
  Time: 12:43
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <title>修改用户信息</title>
</head>
<body>
<form action="/user/updateUser" method="post">
    <label>用户id</label> <br>
    <input type="text" name="id" value="${user.id}"/><br>
    <label>用户名</label><br>
    <input type="text" name="name" value="${user.name}"/><br>
    <label>密码</label><br>
    <input type="password" name="password" value="${user.password}"/>
    <input type="submit" value="提交修改">
</form>
</body>
</html>
