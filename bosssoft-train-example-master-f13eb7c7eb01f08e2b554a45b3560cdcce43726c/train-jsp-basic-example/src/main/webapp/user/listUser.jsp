<%@ page import="com.bosssoft.hr.train.jsp.example.pojo.User" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="UserTag" uri="/WEB-INF/UserTag.tld" %>
<%--
  Created by IntelliJ IDEA.
  User: 62706
  Date: 2020/7/15
  Time: 10:54
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <title>用户管理</title>
</head>

<body>
<UserTag:curUser></UserTag:curUser>
<h4>当前在线人数：<%=application.getAttribute("onLineUser") %>
</h4>
<a href="/user/addUser">新建用户</a>
<h4>数据库中的用户列表：</h4>
<table border="1">
    <tr>
        <th>用户id</th>
        <th>用户名</th>
    </tr>
    <c:forEach items="${userList}" var="user">
        <tr>
            <td>
                <c:out value="${user.id}"></c:out>
            </td>
            <td>
                <c:out value="${user.name}"></c:out>
            </td>
            <td><a href="/user/removeUser?id=${user.id}">删除</a></td>
            <td><a href="/user/updateUser?id=${user.id}">修改</a></td>
        </tr>

    </c:forEach>
</table>

<form action="/logout.jsp" method="get">
    <input type="submit" value="注销">
</form>
</body>
</html>
