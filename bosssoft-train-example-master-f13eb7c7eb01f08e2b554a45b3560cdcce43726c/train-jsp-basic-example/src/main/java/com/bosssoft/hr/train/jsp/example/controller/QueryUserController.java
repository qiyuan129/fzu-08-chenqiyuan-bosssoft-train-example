package com.bosssoft.hr.train.jsp.example.controller;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @description:为了方便展示，用户查询功能以展示用户列表的方式实现，此类没有实现
 * @author: Chen Qiyuan
 * @create: 2020-05-30 11:22
 * @since
 **/
@WebServlet(urlPatterns = {"/user/queryUser"})
public class QueryUserController extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //
    }
}
