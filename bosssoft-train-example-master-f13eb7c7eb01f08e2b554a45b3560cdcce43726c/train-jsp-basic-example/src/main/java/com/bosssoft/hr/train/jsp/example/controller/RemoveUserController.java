package com.bosssoft.hr.train.jsp.example.controller;

import com.bosssoft.hr.train.jsp.example.pojo.User;
import com.bosssoft.hr.train.jsp.example.service.UserService;
import com.bosssoft.hr.train.jsp.example.service.impl.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @param
 * @description:
 * @author: Administrator
 * @create: 2020-05-30 11:22
 * @since
 **/
@WebServlet(urlPatterns = {"/user/removeUser"})
public class RemoveUserController extends HttpServlet {
    private static UserService userService=new UserServiceImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        User user=new User();
        int id= Integer.parseInt(req.getParameter("id"));
        user.setId(id);
        if(userService.remove(user)==true){
            resp.sendRedirect("/user/listUser");
        }
        else{
            req.setAttribute("errorMessage","删除用户失败");
            req.getRequestDispatcher("/error.jsp").forward(req,resp);
        }

    }
}
