package com.bosssoft.hr.train.jsp.example.controller;

import com.bosssoft.hr.train.jsp.example.dao.UserDao;
import com.bosssoft.hr.train.jsp.example.dao.daoImpl.UserDaoImpl;
import com.bosssoft.hr.train.jsp.example.pojo.User;
import com.bosssoft.hr.train.jsp.example.service.UserService;
import com.bosssoft.hr.train.jsp.example.service.impl.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * @param
 * @description:
 * @author: Administrator
 * @create: 2020-05-30 10:11
 * @since
 **/

@WebServlet(urlPatterns = {"/loginServlet"})
public class LoginController extends HttpServlet {
    private static UserService userService=new UserServiceImpl();
    private static UserDao userDao=new UserDaoImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/login.jsp").forward(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int id= Integer.parseInt(req.getParameter("id"));
        String password=req.getParameter("password");
        User user=new User();
        user.setId(id);
        user.setPassword(password);

        if(userService.authentication(user)==true){
            resp.setContentType("text/html;charset=utf-8");
            resp.getWriter().write("login success");
            HttpSession session = req.getSession();
            session.setAttribute("id", id);
            session.setAttribute("name",userDao.selectById(id).getName());

        }
        else{
            resp.setContentType("text/html;charset=utf-8");
            resp.getWriter().write("login failed");
        }

    }


}
