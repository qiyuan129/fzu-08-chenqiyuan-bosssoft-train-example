package com.bosssoft.hr.train.jsp.example.controller;

import com.bosssoft.hr.train.jsp.example.pojo.User;
import com.bosssoft.hr.train.jsp.example.service.UserService;
import com.bosssoft.hr.train.jsp.example.service.impl.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @param
 * @description:
 * @author: Administrator
 * @create: 2020-05-30 11:21
 * @since
 **/
@WebServlet(urlPatterns = {"/user/addUser"})
public class AddUserController extends HttpServlet {
    /**
     *  用户对象
     */
    private static UserService userService=new UserServiceImpl();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/user/addUser.jsp").forward(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //校验参数合法性如果没问题才调用
        User user=new User();
        String idParameter=req.getParameter("id");
        String nameParameter=req.getParameter("name");
        String passwordParameter=req.getParameter("password");

        if (idParameter!=null && nameParameter!=null && passwordParameter!=null){
            try{
                user.setId(Integer.valueOf(idParameter));
                user.setName(nameParameter);
                user.setPassword(passwordParameter);
                if(userService.save(user)==true){
                    resp.sendRedirect("/user/listUser");
                }
                else{
                    req.setAttribute("errorMessage","保存用户失败");
                    req.getRequestDispatcher("/error.jsp").forward(req,resp);
                }

            }catch (Exception ex){
                ex.printStackTrace();
            }
        }else{
            req.setAttribute("errorMessage","保存用户失败，输入参数不合法");
            req.getRequestDispatcher("/error.jsp").forward(req,resp);
             // 错误应答或者跳转错误页面
        }


    }

}
