package com.bosssoft.hr.train.jsp.example.pojo;

import lombok.Data;

/**
 * @param
 * @description:
 * @author: Administrator
 * @create: 2020-05-29 14:09
 * @since
 **/
@Data
public class Query {
    String name;
}
