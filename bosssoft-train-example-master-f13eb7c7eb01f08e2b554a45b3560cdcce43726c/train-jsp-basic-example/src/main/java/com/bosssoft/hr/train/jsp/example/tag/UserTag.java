package com.bosssoft.hr.train.jsp.example.tag;

import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.io.StringWriter;

/**
 * @description: 定义<boss:userTag /> 标签
 * @author: Administrator
 * @create: 2020-05-29 13:50
 * @since
 **/
public class UserTag extends TagSupport {
    StringWriter stringWriter=new StringWriter();


    @Override
    public int doStartTag() {
        try{
            HttpSession session=pageContext.getSession();
            JspWriter out=pageContext.getOut();
            Integer id= (Integer) session.getAttribute("id");
            String name= (String) session.getAttribute("name");
            if(id!=null && name!=null){
                out.println("当前登录用户id；"+id+",用户名:"+name);
            }
            else{
                out.println("从session读取当前用户信息出错!您可能未登录！");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return SKIP_BODY;
    }
}
