package com.bosssoft.hr.train.jsp.example.dao.daoImpl;

import com.bosssoft.hr.train.jsp.example.dao.UserDao;
import com.bosssoft.hr.train.jsp.example.pojo.Query;
import com.bosssoft.hr.train.jsp.example.pojo.User;
import com.bosssoft.hr.train.jsp.example.util.DBUtil;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @description:
 * @author: Administrator
 * @create: 2020-05-30 10:42
 * @since
 **/
public class UserDaoImpl implements UserDao {

    @Override
    public User selectById(Integer id) {
        User user=null;
        Connection conn=null;
        PreparedStatement preparedStatement=null;
        ResultSet rs=null;

        try{
            String querySql="SELECT * FROM t_user WHERE id=?";
            conn=DBUtil.getConnection();
            preparedStatement=conn.prepareStatement(querySql);

            preparedStatement.setInt(1,id);

            rs=preparedStatement.executeQuery();
            while(rs.next()){
                user=new User(rs.getInt("id"),rs.getString("name"),rs.getString("password"));
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        finally {
            DBUtil.close(conn,preparedStatement,rs);
        }
        return user;
    }

    @Override
    public int insert(User user) {
        Connection conn=null;
        PreparedStatement preparedStatement=null;

        try {
            String insertSql="INSERT INTO t_user(id,name,password) VALUES (?,?,?)";
            conn = DBUtil.getConnection();
            preparedStatement=conn.prepareStatement(insertSql);

            preparedStatement.setInt(1,user.getId());
            preparedStatement.setString(2,user.getName());
            preparedStatement.setString(3,user.getPassword());

            return preparedStatement.executeUpdate();

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        finally {
            DBUtil.close(conn,preparedStatement);
        }

        return 0;
    }


    @Override
    public int deleteById(Integer id) {
        Connection conn=null;
        PreparedStatement preparedStatement=null;

        try{
            String deleteSql="DELETE FROM t_user WHERE id=?";
            conn=DBUtil.getConnection();
            preparedStatement=conn.prepareStatement(deleteSql);

            preparedStatement.setInt(1,id);

            return preparedStatement.executeUpdate();

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        finally {
            DBUtil.close(conn,preparedStatement);
        }
        return 0;
    }

    @Override
    public int update(User user) {
        Connection conn=null;
        PreparedStatement preparedStatement=null;

        try{
            String updateSql="UPDATE t_user SET name=?,password=? WHERE id=?";
            conn=DBUtil.getConnection();
            preparedStatement=conn.prepareStatement(updateSql);

            preparedStatement.setInt(3,user.getId());
            preparedStatement.setString(1,user.getName());
            preparedStatement.setString(2,user.getPassword());

            return preparedStatement.executeUpdate();

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        finally {
            DBUtil.close(conn,preparedStatement);
        }
        return 0;
    }

    @Override
    public List<User> queryByCondition(Query queryCondition) {
        List<User> userList=new ArrayList<>();
        Connection conn=null;
        PreparedStatement preparedStatement=null;
        ResultSet rs=null;

        try{
            String querySql="SELECT * FROM t_user WHERE name like %?%";
            conn=DBUtil.getConnection();
            preparedStatement=conn.prepareStatement(querySql);

            preparedStatement.setString(1,queryCondition.getName());

            rs=preparedStatement.executeQuery();
            while(rs.next()){
                User user=new User(rs.getInt("id"),rs.getString("name"),rs.getString("password"));
                userList.add(user);
            }

            return userList;

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        finally {
            DBUtil.close(conn,preparedStatement,rs);
        }

        return userList;
    }

    @Override
    public List<User> listUser() {
        List<User> userList=new ArrayList<>();
        Connection conn=null;
        Statement statement=null;
        ResultSet rs=null;

        try{
            String querySql="SELECT * FROM t_user";
            conn=DBUtil.getConnection();
            statement=conn.createStatement();

            rs=statement.executeQuery(querySql);
            while(rs.next()){
                User user=new User(rs.getInt("id"),rs.getString("name"));
                userList.add(user);
            }

            return userList;

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        finally {
            DBUtil.close(conn,statement,rs);
        }

        return userList;
    }
}
