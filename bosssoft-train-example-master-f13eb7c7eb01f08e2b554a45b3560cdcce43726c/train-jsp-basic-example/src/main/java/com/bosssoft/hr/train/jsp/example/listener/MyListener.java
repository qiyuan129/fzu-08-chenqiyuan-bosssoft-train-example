package com.bosssoft.hr.train.jsp.example.listener;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.http.HttpSessionAttributeListener;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

public class MyListener implements ServletContextListener, HttpSessionAttributeListener, HttpSessionListener {
    ServletContext application = null;
    @Override
    public void contextDestroyed(ServletContextEvent event) {
        System.out.println("服务器关闭");
    }

    @Override
    public void contextInitialized(ServletContextEvent event) {
    }

    @Override
    public void attributeAdded(HttpSessionBindingEvent se) {
    }

    @Override
    public void attributeRemoved(HttpSessionBindingEvent se) {

    }

    @Override
    public void attributeReplaced(HttpSessionBindingEvent se) {

    }

    @Override
    public void sessionCreated(HttpSessionEvent event)
    {
        ServletContext sct = event.getSession().getServletContext();

        Integer onLineUser = (Integer) sct.getAttribute("onLineUser");
        if (null == onLineUser) {
            //不知道为什么，服务器一启动，在自动打开一个页面后，就已经自动创建了三个session，所以这里先初始化为-1令显示的在线人数正确
            onLineUser = Integer.valueOf(0);
        }else {
            int count = onLineUser.intValue();
            onLineUser = Integer.valueOf(count+1);
        }
        sct.setAttribute("onLineUser", onLineUser);

        System.out.println("服务器端建立了一个session");
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent event)
    {
        ServletContext sct = event.getSession().getServletContext();
        Integer onLineUser = (Integer) sct.getAttribute("onLineUser");
        if (null == onLineUser) {

            onLineUser = Integer.valueOf(0);
        }else {
            int count = onLineUser.intValue();
            onLineUser = Integer.valueOf(count-1);
        }
        sct.setAttribute("onLineUser", onLineUser);

        System.out.println("服务器端销毁了一个session");
    }
}
