package com.bosssoft.hr.train.jsp.example.service.impl;

import com.bosssoft.hr.train.jsp.example.dao.UserDao;
import com.bosssoft.hr.train.jsp.example.dao.daoImpl.UserDaoImpl;
import com.bosssoft.hr.train.jsp.example.exception.BusinessException;
import com.bosssoft.hr.train.jsp.example.pojo.Query;
import com.bosssoft.hr.train.jsp.example.pojo.User;
import com.bosssoft.hr.train.jsp.example.service.UserService;

import java.util.ArrayList;
import java.util.List;

/**
 * @description:
 * @author: Administrator
 * @create: 2020-05-30 10:24
 * @since
 **/

public class UserServiceImpl implements UserService {
    UserDao userDao=new UserDaoImpl();

    @Override
    public boolean save(User user) {
        try {
            if(user==null){
                return false;
            }
            else{
                if(userDao.selectById(user.getId())!=null){
                    return false;
                }
                else{
                    userDao.insert(user);
                    return true;
                }
            }
        }catch (Exception ex){
            throw new BusinessException("10001",ex.getMessage(),ex);
        }

    }

    @Override
    public boolean remove(User user) {
        if(user!=null){
            userDao.deleteById(user.getId());
            return true;
        }
        return false;
    }

    @Override
    public boolean update(User user) {
        if(user!=null){
            if(userDao.update(user)>0) {
                return true;
            }
            else{
                return false;
            }
        }
        return false;
    }

    @Override
    public List<User> queryByCondition(Query queryCondition) {
        if(queryCondition!=null){
            List<User> userList=userDao.queryByCondition(queryCondition);
            return userList;
        }
        else{
            return new ArrayList<User>();
        }
    }

    @Override
    public boolean authentication(User user) {
        if(user!=null){
            User selectedUser=userDao.selectById(user.getId());

            if(selectedUser==null){
                return false;
            }
            else{
                String givenPassword=user.getPassword();
                String rightPassword=selectedUser.getPassword();

                if(givenPassword==null || rightPassword ==null){
                    return false;
                }
                else{
                    if(givenPassword.equals(rightPassword)){
                        return true;
                    }
                    else{
                        return false;
                    }
                }
            }

        }
        return false;
    }
}
