package com.bosssoft.hr.train.j2se.basic.example.collection;

import com.bosssoft.hr.train.j2se.basic.example.collection.impl.LinkedListExampleImpl;
import com.bosssoft.hr.train.j2se.basic.example.pojo.User;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class LinkedListExampleImplTest {
    LinkedListExampleImpl linkedListExmaple;
    User user1;
    User user2;
    User user3;
    User user4;

    @Before
    public void setUp() throws Exception {
        linkedListExmaple=new LinkedListExampleImpl();
        user1=new User(1,"Kiana");
        user2=new User(2,"Bronya");
        user3=new User(3,"Fu Hua");
        user4=new User(4,"Raiden Mei");
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void append() {
        linkedListExmaple.append(user1);
    }

    @Test
    public void get() {
        linkedListExmaple.append(user1);
        User actualUser=linkedListExmaple.get(0);
        Assert.assertEquals(user1,actualUser);
    }

    @Test
    public void insert() {
        linkedListExmaple.append(user1);
        linkedListExmaple.append(user3);
        linkedListExmaple.insert(1,user2);
        User actualUser=linkedListExmaple.get(1);
        Assert.assertEquals(user2,actualUser);
    }

    @Test
    public void remove() {
        linkedListExmaple.append(user1);
        linkedListExmaple.append(user2);
        linkedListExmaple.remove(1);
    }

    @Test
    public void listByIndex() {
        linkedListExmaple.append(user1);
        linkedListExmaple.append(user2);
        linkedListExmaple.append(user3);
        linkedListExmaple.listByIndex();
    }

    @Test
    public void listByIterator() {
        linkedListExmaple.append(user1);
        linkedListExmaple.append(user2);
        linkedListExmaple.append(user3);
        linkedListExmaple.listByIterator();
    }

    @Test
    public void toArray() {
        linkedListExmaple.append(user1);
        linkedListExmaple.append(user2);
        User[] userArray=linkedListExmaple.toArray();
    }

    @Test
    public void sort() {
        linkedListExmaple.append(user1);
        linkedListExmaple.append(user3);
        linkedListExmaple.append(user2);
        linkedListExmaple.sort();

        User secondUser=linkedListExmaple.get(1);
        Assert.assertEquals(user2,secondUser);
    }

    @Test
    public void sort2() {
        linkedListExmaple.append(user1);
        linkedListExmaple.append(user3);
        linkedListExmaple.append(user2);
        linkedListExmaple.sort2();

        User secondUser=linkedListExmaple.get(1);
        Assert.assertEquals(user2,secondUser);
    }

    @Test
    public void addFirst() {
        linkedListExmaple.append(user1);
        linkedListExmaple.append(user2);
        linkedListExmaple.addFirst(user3);

        User firstUser=linkedListExmaple.get(0);
        Assert.assertEquals(user3,firstUser);
    }

    @Test
    public void offer() {
        linkedListExmaple.append(user1);
        linkedListExmaple.append(user2);
        linkedListExmaple.offer(user3);

        User lastUser=linkedListExmaple.get(2);
        Assert.assertEquals(user3,lastUser);
    }

    @Test
    public void sychronizedVisit() {
        linkedListExmaple.sychronizedVisit(user1);
    }

    @Test
    public void push() {
        linkedListExmaple.push(user1);
        linkedListExmaple.push(user2);
    }

    @Test
    public void pop() {
        linkedListExmaple.push(user1);
        linkedListExmaple.push(user2);
        User actualUser=linkedListExmaple.pop();
        Assert.assertEquals(user2,actualUser);
    }
}