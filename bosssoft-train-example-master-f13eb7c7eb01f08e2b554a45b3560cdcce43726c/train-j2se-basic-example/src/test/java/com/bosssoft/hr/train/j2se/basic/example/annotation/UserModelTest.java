package com.bosssoft.hr.train.j2se.basic.example.annotation;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class UserModelTest {
    UserModel user1;
    UserModel user2;
    UserModel user3;

    @Before
    public void setUp() throws Exception {
        user1 =new UserModel(1l,"student1",23);
        user2 =new UserModel(2l,"学生2",22);
        user3 =new UserModel(3l,"学生3",24);
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void save() {
        user1.save();
        user2.save();
        user3.save();
    }

    @Test
    public void update() {
        user2.setName("学生2——修改后");
        user2.update();
    }

    @Test
    public void remove() {
        user1.remove();
        user2.remove();
        user3.remove();
    }

    @Test
    public void queryForList() {
        //比较麻烦暂时没有实现
    }
}