package com.bosssoft.hr.train.j2se.basic.example.xml;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.xml.sax.SAXException;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;

public class SAXOperatironTest {
    SAXParserFactory factory;
    SAXParser saxParser;
    SAXOperatiron saxOperatiron;
    @Before
    public void setUp() throws Exception {
        factory=SAXParserFactory.newInstance();
        saxParser=factory.newSAXParser();
        saxOperatiron=new SAXOperatiron();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void query()  {
        try {
            saxParser.parse("src\\test\\resources\\student.tld",saxOperatiron);
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}