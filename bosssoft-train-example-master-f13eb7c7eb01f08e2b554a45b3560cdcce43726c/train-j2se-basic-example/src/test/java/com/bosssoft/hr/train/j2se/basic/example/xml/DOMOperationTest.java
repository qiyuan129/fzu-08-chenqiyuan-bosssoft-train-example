package com.bosssoft.hr.train.j2se.basic.example.xml;

import com.bosssoft.hr.train.j2se.basic.example.pojo.Student;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.*;

public class DOMOperationTest {
    XMLOperation xmlOperation;
    final String TEST_FILE_PATH ="src\\test\\resources\\student.tld";
    //测试用的临时文件
    final String TEST_FILE_RUNTIME_PATH ="src\\test\\resources\\testRuntimeResources\\student.tld";

    @Before
    public void setUp() throws Exception {
        InputStream in=null;
        OutputStream out=null;

        try{
            //初始化（复制一份原始文件作为测试用的临时文件）
            in=new FileInputStream(new File(TEST_FILE_PATH));
            out=new FileOutputStream(new File(TEST_FILE_RUNTIME_PATH));

            byte[] buffer=new byte[1024];
            int len;

            while((len=in.read(buffer))>0){
                out.write(buffer,0,len);
            }

        }
        catch (Exception e){
            e.printStackTrace();
        }
        finally {
            if(in!=null){
                in.close();
            }
            if(out!=null){
                out.close();
            }
        }

        xmlOperation=new DOMOperation(TEST_FILE_RUNTIME_PATH);
    }

    @After
    public void tearDown() throws Exception {
        File testFile=new File(TEST_FILE_RUNTIME_PATH);
        if(testFile.exists() && testFile.isFile()){
            if (testFile.delete()==false) {
                System.out.println("删除测试用临时文件失败！");
            }
        }
        else {
            System.out.println("删除测试用临时文件失败！文件不存在");
        }

    }

    @Test
    public void create() {
        Student student=new Student(1,"学生1",21);
        xmlOperation.create(student);
    }

    @Test
    public void remove() {
        Student student=new Student();
        student.setId(2);
        xmlOperation.remove(student);
    }

    @Test
    public void update() {
        Student student=new Student(3,"student3_modified",23);
        xmlOperation.update(student);
    }

    @Test
    public void query() {
        Student student=new Student(3,"student3",23);

        Student queryResult=xmlOperation.query(student);
        Assert.assertEquals(student,queryResult);
    }
}