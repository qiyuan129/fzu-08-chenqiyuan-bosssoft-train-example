//package com.bosssoft.hr.train.j2se.basic.example.thread;
//
//
//import org.junit.Before;
//import org.junit.Test;
//
//import java.util.concurrent.*;
//
//public class ThreadPoolTest {
//    private static final int NTHREADS=20;
//    ThreadPoolExecutor executor;
//
//    @Before
//    public void setUp(){
//        BlockingQueue<Runnable> blockingQueue=new LinkedBlockingQueue<>();
//        executor=new ThreadPoolExecutor(3,6,2, TimeUnit.MILLISECONDS,blockingQueue);
//    }
//
//    @Test
//    public void runThreadTest(){
//        for(int i=0;i<NTHREADS;i++){
//            Runnable worker=new MyThread();
//            executor.execute(worker);
//        }
//        executor.shutdown();
//
//        while(!executor.isTerminated()){}
//        System.out.println("所有线程均已结束");
//    }
//
//    @Test
//    public void runFutureTaskTest(){
//        Task task=new Task();
//        FutureTask<Integer> futureTask=new FutureTask<>(task);
//        executor.submit(futureTask);
//        executor.shutdown();
//
////        try{
////            Thread.sleep(1000);
////        }
////        catch (InterruptedException e) {
////            e.printStackTrace();
////        }
//
//        System.out.println("主线程正在执行");
//
//        try {
//            if(futureTask.get()!=null){
//                System.out.println("task运行结果"+futureTask.get());
//            }else{
//                System.out.println("future.get()未获取到结果");
//            }
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        } catch (ExecutionException e) {
//            e.printStackTrace();
//        }
//
//        System.out.println("所有任务执行完毕");
//    }
//}
