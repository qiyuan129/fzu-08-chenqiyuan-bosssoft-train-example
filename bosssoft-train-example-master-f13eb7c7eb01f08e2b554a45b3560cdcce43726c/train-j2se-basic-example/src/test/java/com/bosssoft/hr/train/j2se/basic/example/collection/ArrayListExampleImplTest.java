package com.bosssoft.hr.train.j2se.basic.example.collection;

import com.bosssoft.hr.train.j2se.basic.example.collection.impl.ArrayListExampleImpl;
import com.bosssoft.hr.train.j2se.basic.example.pojo.User;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ArrayListExampleImplTest {
    ArrayListExmaple<User> arrayListExample;
    User user1;
    User user2;
    User user3;
    User user4;

    @Before
    public void setUp() throws Exception {
        arrayListExample=new ArrayListExampleImpl();

        user1=new User(1,"Kiana");
        user2=new User(2,"Bronya");
        user3=new User(3,"Fu Hua");
        user4=new User(4,"Raiden Mei");
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void append() {
        arrayListExample.append(user1);
    }

    @Test
    public void get() {
        arrayListExample.append(user1);
        User user=arrayListExample.get(0);
        Assert.assertNotNull(user);
    }

    @Test
    public void insert() {
        arrayListExample.append(user1);
        arrayListExample.append(user2);
        arrayListExample.insert(1,user3);
        User actualUser=arrayListExample.get(1);
        Assert.assertEquals(user3,actualUser);
    }

    @Test
    public void remove() {
        arrayListExample.append(user1);
        arrayListExample.append(user2);
        arrayListExample.append(user3);
        arrayListExample.remove(1);

        //移除下标1处的user后，1的位置应该为user3
        User actualUser=arrayListExample.get(1);
        Assert.assertEquals(user3,actualUser);
    }

    @Test
    public void listByIndex() {
        arrayListExample.append(user1);
        arrayListExample.append(user2);
        arrayListExample.append(user3);
        arrayListExample.listByIndex();
    }

    @Test
    public void listByIterator() {
        arrayListExample.append(user1);
        arrayListExample.append(user2);
        arrayListExample.listByIterator();
    }

    @Test
    public void toArray() {
        arrayListExample.append(user1);
        arrayListExample.append(user2);
        arrayListExample.append(user3);

        User[] userArray=arrayListExample.toArray();
        Assert.assertNotNull(userArray);
    }

    @Test
    public void sort() {
        arrayListExample.append(user1);
        arrayListExample.append(user3);
        arrayListExample.append(user2);
        arrayListExample.sort();

        User secondUser=arrayListExample.get(1);
        Assert.assertEquals(user2,secondUser);
    }

}