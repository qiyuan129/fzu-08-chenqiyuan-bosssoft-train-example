package com.bosssoft.hr.train.j2se.basic.example.collection;

import com.bosssoft.hr.train.j2se.basic.example.collection.impl.TreeSetExampleImpl;
import com.bosssoft.hr.train.j2se.basic.example.pojo.User;
import org.junit.Before;
import org.junit.Test;

public class TreeSetExampleImplTest {
    TreeSetExmaple treeSetExmaple;
    @Before
    public void setUp(){
        treeSetExmaple=new TreeSetExampleImpl();

    }
    @Test
    public void sort() {
        User user1=new User(1,"Kiana");
        User user2=new User(2,"Bronya");
        User user3=new User(3,"Fu Hua");
        User user4=new User(4,"Raiden Mei");

        User[] userArray={user1,user3,user2,user4};
        treeSetExmaple.sort(userArray);
    }
}