package com.bosssoft.hr.train.j2se.basic.example.collection;

import com.bosssoft.hr.train.j2se.basic.example.collection.impl.HashMapExampleImpl;
import com.bosssoft.hr.train.j2se.basic.example.pojo.Resource;
import com.bosssoft.hr.train.j2se.basic.example.pojo.Role;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class HashMapExampleImplTest {
    Role role1;
    Role role2;
    Role role3;
    Resource resource1;
    Resource resource2;
    Resource resource3;
    HashMapExample hashMapExample;

    @Before
    public void setUp() throws Exception {
        hashMapExample=new HashMapExampleImpl();

        role1=new Role(1,"角色1");
        role2=new Role(2,"角色2");
        role3=new Role(3,"角色3");
        resource1=new Resource(1,"资源1");
        resource2=new Resource(2,"资源2");
        resource3=new Resource(3,"资源3");
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void put() {
        hashMapExample.put(role1,resource1);
    }

    @Test
    public void remove() {
        hashMapExample.put(role1,resource1);
        hashMapExample.remove(role1);
    }

    @Test
    public void containsKey() {
        hashMapExample.put(role1,resource1);
        Assert.assertTrue(hashMapExample.containsKey(role1));
    }

    @Test
    public void visitByEntryset() {
        hashMapExample.put(role1,resource1);
        hashMapExample.put(role2,resource2);
        hashMapExample.put(role3,resource3);
        hashMapExample.visitByEntryset();
    }

    @Test
    public void visitByKeyset() {
        hashMapExample.put(role1,resource1);
        hashMapExample.put(role2,resource2);
        hashMapExample.put(role3,resource3);
        hashMapExample.visitByKeyset();
    }

    @Test
    public void visitByValues() {
        hashMapExample.put(role1,resource1);
        hashMapExample.put(role2,resource2);
        hashMapExample.put(role3,resource3);
        hashMapExample.visitByValues();
    }
}