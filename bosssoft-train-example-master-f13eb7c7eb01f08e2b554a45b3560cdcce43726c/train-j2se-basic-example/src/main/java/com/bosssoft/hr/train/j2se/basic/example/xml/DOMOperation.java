package com.bosssoft.hr.train.j2se.basic.example.xml;

import com.bosssoft.hr.train.j2se.basic.example.pojo.Student;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;

/**
 * @param
 * @description:
 * @author: Administrator
 * @create: 2020-05-28 22:13
 * @since
 **/
public class DOMOperation implements XMLOperation<Student> {
    Document document;
    String xmlPath;

    public DOMOperation(String documentPath){
        try {
            DocumentBuilderFactory factory= DocumentBuilderFactory.newInstance();
            DocumentBuilder builder=factory.newDocumentBuilder();
            document=builder.parse(documentPath);
            this.xmlPath=documentPath;

        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    @Override
    public boolean create(Student object) {
        Element studentList= document.getDocumentElement();

        //开始构造新student节点
        Element student=document.createElement("student");
        student.setAttribute("id",object.getId().toString());

        Element age=document.createElement("age");
        age.setTextContent(object.getAge().toString());
        Element name=document.createElement("name");
        name.setTextContent(object.getName());

        student.appendChild(age);
        student.appendChild(name);

        studentList.appendChild(student);

        try {
            TransformerFactory factory = TransformerFactory.newInstance();
            Transformer former = null;
            former = factory.newTransformer();
            former.transform(new DOMSource(document), new StreamResult(new File(
                    xmlPath)));
        }
        catch (TransformerConfigurationException e) {
            e.printStackTrace();
        }
        catch (TransformerException e) {
            e.printStackTrace();
        }


        return true;
    }

    @Override
    public boolean remove(Student object) {
        try{
            Element root=document.getDocumentElement();
            NodeList studentList=root.getChildNodes();
            Element selectedElement=null;
            // 定位根节点中指定id的节点

            for(int i=0;i<studentList.getLength();i++){
                if(studentList.item(i).getNodeType()==Node.ELEMENT_NODE){
                    Element studentElement=(Element)studentList.item(i);

                    int elementId= Integer.parseInt(studentElement.getAttribute("id"));

                    if(elementId==object.getId()){
                        selectedElement=studentElement;
                        break;
                    }
                }

            }


            // 删除该节点
            if(selectedElement!=null) {
                root.removeChild(selectedElement);
            }
            else{
                return false;
            }
            // 保存
            TransformerFactory factory = TransformerFactory.newInstance();
            Transformer former = factory.newTransformer();
            former.transform(new DOMSource(document), new StreamResult(new File(
                    xmlPath)));
        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        }

        return false;
    }

    @Override
    public boolean update(Student object) {
        Element students=document.getDocumentElement();
        NodeList studentList=students.getChildNodes();

        //遍历students标签的子标签
        for(int i=0;i<studentList.getLength();i++){


            if(studentList.item(i).getNodeType()==Node.ELEMENT_NODE){
                Element studentElement=(Element)studentList.item(i);
                int elementId= Integer.parseInt(studentElement.getAttribute("id"));

                //当找到指定id的student后开始修改学生详细信息
                if(elementId==object.getId()){
                    String name = object.getName();
                    Integer age=object.getId();

                    //循环节点内的所有子节点
                    for(Node node=studentElement.getFirstChild();node!=null;node=node.getNextSibling()){
                        if("name".equals(node.getNodeName())){
                            node.getFirstChild().setNodeValue(name);
                            continue;
                        }
                        if("age".equals(node.getNodeName()) && node.getFirstChild().getNodeValue()!=null){
                            node.getFirstChild().setNodeValue(age.toString());
                        }

                    }
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public Student query(Student object) {
        Element students=document.getDocumentElement();
        NodeList studentList=students.getChildNodes();

        //遍历students标签的子标签
        for(int i=0;i<studentList.getLength();i++){


            if(studentList.item(i).getNodeType()==Node.ELEMENT_NODE){
                Element studentElement=(Element)studentList.item(i);
                int elementId= Integer.parseInt(studentElement.getAttribute("id"));

                //当找到指定id的student后开始获取学生详细信息
                if(elementId==object.getId()){
                    String name = null;
                    Integer age=null;

                    //循环节点内的所有子节点
                    for(Node node=studentElement.getFirstChild();node!=null;node=node.getNextSibling()){
                        if("name".equals(node.getNodeName())){
                            name=node.getFirstChild().getNodeValue();
                            continue;
                        }
                        if("age".equals(node.getNodeName()) && node.getFirstChild().getNodeValue()!=null){
                                age = Integer.parseInt(node.getFirstChild().getNodeValue());
                        }

                    }
                    return new Student(elementId,name,age);
                }
            }
        }
            //遍历完还没找到就返回null
        return null;
    }

}
