package com.bosssoft.hr.train.j2se.basic.example.socket;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

/**
 * @param
 * @author: Administrator
 * @create: 2020-05-28 22:25
 * @since
 **/
public class ClientSocket implements Starter {
    @Override
    public boolean start() {
        try (SocketChannel socketChannel = SocketChannel.open()) {
            SocketAddress socketAddress = new InetSocketAddress("127.0.0.1", 8888);
            socketChannel.connect(socketAddress);

            ByteBuffer buffer = ByteBuffer.allocate(1024);

            buffer.clear();
            //向服务端发送消息
            buffer.put("hello server".getBytes());
            //读取模式
            buffer.flip();
            socketChannel.write(buffer);
            buffer.clear();

            //从服务端读取消息
            int readLenth = socketChannel.read(buffer);
            //读取模式
            buffer.flip();
            byte[] bytes = new byte[readLenth];
            buffer.get(bytes);
            System.out.println(new String(bytes, "UTF-8"));
            buffer.clear();

        } catch (IOException e) {
            e.printStackTrace();
        }

        return false;
    }

}
