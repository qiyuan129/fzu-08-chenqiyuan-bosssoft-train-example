package com.bosssoft.hr.train.j2se.basic.example.database;

import java.sql.*;

/**
 * @description:  我是工具类并且我不喜欢被继承 final 保护了我免于继承，private 保护我被创建
 * @author: Administrator
 * @create: 2020-05-28 20:45
 * @since
 **/
public final class DBUtil {
    private final static String DB_URL="jdbc:mysql://39.100.48.69:3306/bosssoft?useSSL=false&serverTimezone=Asia/Shanghai&"+
                "characterEncoding=UTF8";
    private final static String USERNAME="root";
    private final static String PASSWORD="Userpassword123!@#";
    private DBUtil(){

    }


    /**
     * 获取与数据库的连接Connection，如果已经建立连接则直接返回
     * @return
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    public static Connection getConnection()  {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection conn= DriverManager.getConnection(DB_URL,USERNAME,PASSWORD);
            return conn;
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     *  关闭数据连接
     * @param con
     * @param sta
     * @param rs
     */
    public static void close(Connection con, Statement sta, ResultSet rs){

        try {
            if(rs !=null) {
                rs.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            if(sta !=null) {
                sta.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            if(con !=null) {
                con.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 关闭数据库连接（不涉及查询的方法）
     * @param con
     * @param sta
     */
    public static void close(Connection con, Statement sta){
        try {
            if(sta !=null) {
                sta.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            if(con !=null) {
                con.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
