package com.bosssoft.hr.train.j2se.basic.example.annotation;

import lombok.Getter;
import lombok.Setter;

/**
 * @param
 * @description:
 * @author: Administrator
 * @create: 2020-05-28 22:42
 * @since
 **/
@Table(value="t_user")
@Getter
@Setter
public class UserModel extends BaseModel {
    @Id(value="id")
    private Long ID;

    @Column(value="name")
    private String name;

    @Column(value="age")
    private Integer age;

    public UserModel(Long ID, String name, Integer age) {
        this.ID = ID;
        this.name = name;
        this.age = age;
    }
}
