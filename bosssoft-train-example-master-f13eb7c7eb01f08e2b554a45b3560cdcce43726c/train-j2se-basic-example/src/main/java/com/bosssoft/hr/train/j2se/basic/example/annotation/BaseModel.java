package com.bosssoft.hr.train.j2se.basic.example.annotation;

import com.bosssoft.hr.train.j2se.basic.example.database.DBUtil;

import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
/**
 * @param
 * @description:
 * @author: Administrator
 * @create: 2020-05-28 22:41
 * @since
 **/
public abstract class BaseModel {
    String SAVE_SQL="INSERT INTO ? (?,?,?) values(?,?,?)";
    String UPDATE_SQL ="UPDATE ? SET id=?,name=?,age=?  WHERE id=?";
    String REMOVE_SQL ="DELETE FROM ? WHERE id=?";
    String QUERY_SQL ="SELECT * FROM ? WHERE id=?";

    public int save()  {
        Class clazz=this.getClass();
        String tableName=null;
        List<String> columns=new ArrayList<>();
        List<String> values=new ArrayList<>();

        Connection conn=null;
        Statement statement=null;

        try {
            conn= DBUtil.getConnection();
            statement=conn.createStatement();

            //获取表名
            if(clazz.isAnnotationPresent(Table.class)){
                Table table= (Table) clazz.getAnnotation(Table.class);
                tableName=table.value();
            }
            else{
                return -1;
            }

            //获取Id及各字段
            Field[] fields=clazz.getDeclaredFields();
            for(Field field:fields){
                field.setAccessible(true);
                if(field.isAnnotationPresent(Id.class)){
                    Id idAnnotation=field.getAnnotation(Id.class);

                    //由注解获取字段名，field对象获取字段值
                    columns.add(idAnnotation.value());
                    values.add(field.get(this).toString());
                }
                if(field.isAnnotationPresent(Column.class)){
                    Column columnAnnotation=field.getAnnotation(Column.class);

                    //由注解获取字段名，field对象获取字段值
                    if(field.getType()==String.class){
                        columns.add(columnAnnotation.value());
                        values.add("\'"+field.get(this).toString()+"\'");
                    }
                    else {
                        columns.add(columnAnnotation.value());
                        values.add(field.get(this).toString());
                    }
                }
            }

            String saveSql=buildSaveSql(tableName,columns,values);
            int rsCount=statement.executeUpdate(saveSql);

            return rsCount;
        }
        catch (SQLException | IllegalAccessException throwables) {
            throwables.printStackTrace();
        }
        finally {
            DBUtil.close(conn,statement);
        }

        return -1;
    }

    public int update(){
        Class clazz=this.getClass();
        String tableName=null;
        String id=null;
        List<String> columns=new ArrayList<>();
        List<String> values=new ArrayList<>();

        Connection conn=null;
        Statement statement=null;

        if(clazz.isAnnotationPresent(Table.class)){
            Table table= (Table) clazz.getAnnotation(Table.class);
            tableName=table.value();
        }
        else{
            return -1;
        }

        try{
            Field[] fields=clazz.getDeclaredFields();
            for(Field field:fields){
                field.setAccessible(true);

                //id字段单独处理
                if(field.isAnnotationPresent(Id.class)){
                    id=field.get(this).toString();
                }

                //处理其他字段
                if(field.isAnnotationPresent(Column.class)){
                    Column columnAnnotation=field.getAnnotation(Column.class);

                    if(field.getType()==String.class){
                        columns.add(columnAnnotation.value());
                        values.add('\''+field.get(this).toString()+'\'');
                    }
                    else{
                        columns.add(columnAnnotation.value());
                        values.add(field.get(this).toString());
                    }
                }
            }

            conn=DBUtil.getConnection();
            statement=conn.createStatement();
            String updateSql=buildUpdateSql(tableName,id,columns,values);
            int resultCount=statement.executeUpdate(updateSql);

            return resultCount;
        }
        catch (IllegalAccessException | SQLException e) {
            e.printStackTrace();
        }
        finally {
            DBUtil.close(conn,statement);
        }


        return -1;
    }

    public int remove(){
        String tableName=null;
        String id=null;

        Connection conn=null;
        Statement statement=null;

        Class clazz=this.getClass();
        if(clazz.isAnnotationPresent(Table.class)){
            Table tableAnnotation= (Table) clazz.getAnnotation(Table.class);
            tableName=tableAnnotation.value();
        }
        else{
            return -1;
        }

        try{
            Field[] fields=clazz.getDeclaredFields();
            for(Field field:fields){
                field.setAccessible(true);
                if(field.isAnnotationPresent(Id.class)){
                    id=field.get(this).toString();
                }
            }

            conn=DBUtil.getConnection();
            statement=conn.createStatement();
            String deleteSql=buildDeleteSql(tableName,id);
            int resultCount=statement.executeUpdate(deleteSql);

            return resultCount;
        }
        catch (IllegalAccessException | SQLException e) {
            e.printStackTrace();
        }
        finally {
            DBUtil.close(conn,statement);
        }


        return 0;
    }

    public List queryForList(){
//        List<Object> list=new ArrayList<>();
//        String tableName=null;
//
//        Class clazz=this.getClass();
//        if(clazz.isAnnotationPresent(Table.class)){
//            Table tableAnnotation= (Table) clazz.getAnnotation(Table.class);
//            tableName=tableAnnotation.value();
//        }
//
//        try{
//            String querySql="SELECT * FROM "+tableName;
//            Connection conn=DBUtil.getConnection();
//            Statement statement=conn.createStatement();
//
//            ResultSet rs=statement.executeQuery(querySql);
//            while(rs.next()){
//                Long id=rs.getLong()
//            }
//        } catch (SQLException throwables) {
//            throwables.printStackTrace();
//        }

        return null;
    }

    /**
     * 构造插入的SQL语句
     * @param tableName  待插入表名
     * @param columns   各列列名
     * @param values   各列的值（字符串形式）
     * @return
     */
    public String buildSaveSql(String tableName,List<String> columns,List<String> values){
        StringBuilder builder=new StringBuilder("INSERT INTO ");
        builder.append(tableName+" (");

        for(int i=0;i<columns.size();i++){
            String column=columns.get(i);
            if(i==0){
                builder.append(column);
            }
            else{
                builder.append(','+column);
            }
        }
        builder.append(") VALUES(");

        for(int i=0;i<values.size();i++){
            String value=values.get(i);
            if(i==0){
                builder.append(value);
            }
            else{
                builder.append(','+value);
            }
        }
        builder.append(")");

        return builder.toString();
    }

    public String buildUpdateSql(String tableName,String id,List<String> columns,List<String> values){
        StringBuilder builder=new StringBuilder("UPDATE "+tableName+" SET ");

        for(int i=0;i<columns.size();i++){
            if(i==0){
                builder.append(columns.get(i)+'='+values.get(i));
            }
            else{
                builder.append(','+columns.get(i)+'='+values.get(i));
            }
        }

        builder.append(" WHERE id="+id);
        return builder.toString();
    }

    public String buildDeleteSql(String tableName,String id){
        StringBuilder builder=new StringBuilder("DELETE FROM "+tableName+" WHERE id="+id);
        return builder.toString();
    }
}
